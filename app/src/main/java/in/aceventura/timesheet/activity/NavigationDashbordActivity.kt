package `in`.aceventura.timesheet.activity

import `in`.aceventura.timesheet.R
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_navigation_dashbord.*

class NavigationDashbordActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation_dashbord)
        ll_dailyAttendance.setOnClickListener { view ->
            startActivity(Intent(this, DailyAttendanceMainActivity::class.java))

        }
        closenav.setOnClickListener { view ->
            finish()

        }
    }
}