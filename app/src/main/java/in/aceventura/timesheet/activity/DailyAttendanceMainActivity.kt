package `in`.aceventura.timesheet.activity

import `in`.aceventura.timesheet.R
import `in`.aceventura.timesheet.adapter.DailyTaskAddAdapter
import `in`.aceventura.timesheet.database.RoomSingleton
import `in`.aceventura.timesheet.model.DailyTaskModel
import `in`.aceventura.timesheet.util.AllValues
import android.os.AsyncTask


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_daily_attendance_main.*

class DailyAttendanceMainActivity : AppCompatActivity(), AllValues {
    lateinit var rv_addDailyTask: RecyclerView
    var S: String = ""

    private lateinit var mDb: RoomSingleton
    lateinit var mprojectAdapter: DailyTaskAddAdapter;
    var listDailyTaskModel = ArrayList<DailyTaskModel>()
    var hoursdummy = ArrayList<String>()
    lateinit var projectListLayoutManager: LinearLayoutManager
    lateinit var cmd: DailyAttendanceMainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daily_attendance_main)
        supportActionBar?.hide()
        cmd = this;
        initView()
        initValue()
        colletionData()
    }

    private fun initValue() {

        Log.e("Valxxxues?>", "00")

        mDb = RoomSingleton.getInstance(this)
        val list = mDb.dailyTaskDao().allStudents()
        Log.e("Valxxxues?>", "0111${list}")

        for (student in list) {
            Log.e("Valxxxues?>", "02")
            Log.e("Valxxxues?>", "02222=${student.dailyhours}")
            Log.e("Valxxxues?>", "03")
        }
        Log.e("Valxxxues?>", "04")
        btaddTask.setOnClickListener { view ->


            projectListLayoutManager = LinearLayoutManager(this)
            rv_addDailyTask.layoutManager = projectListLayoutManager

            var mdailyTaskModel = DailyTaskModel();
            mdailyTaskModel.dailyEditableTask = ""

            listDailyTaskModel.add(mdailyTaskModel)

            mprojectAdapter = DailyTaskAddAdapter(listDailyTaskModel, hoursdummy, this, this)

            rv_addDailyTask.adapter = mprojectAdapter



            for (ss in hoursdummy) {
                Log.e("Values?>", "Values>$ss")

            }


        }

    }

    private fun initView() {
        rv_addDailyTask = findViewById(R.id.rv_addDailyTask)

    }

    private fun colletionData() {

        val marks = arrayOf(hoursdummy)
        for (item in marks) {
            println(item)
            //ettotalhr.setText(item)
            ettotalhr.setText(item.toString())
            Toast.makeText(this, "" + item, Toast.LENGTH_LONG).show()


        }


    }

    override fun addHour(ss: String) {
        Log.e("interface", "calling=$ss")
        //  ettotalhr.setText(ss.toString())
    }


}

/* private fun initValue() {
    btaddTask.setOnClickListener { view ->


         projectListLayoutManager = LinearLayoutManager(this)
         rv_addDailyTask.layoutManager = projectListLayoutManager

         var mdailyTaskModel = DailyTaskModel();
         mdailyTaskModel.dailyEditableTask=""

         listDailyTaskModel.add(mdailyTaskModel)

         mprojectAdapter = DailyTaskAddAdapter(listDailyTaskModel,hoursdummy,this)

         rv_addDailyTask.adapter = mprojectAdapter


     }
 }
*/
/*  private fun initView() {
      rv_addDailyTask = findViewById(R.id.rv_addDailyTask)



  }*/

