package `in`.aceventura.timesheet.activity

import `in`.aceventura.timesheet.R
import `in`.aceventura.timesheet.network.VolleySingleton
import `in`.aceventura.timesheet.util.Config.NEW_LOGIN
import `in`.aceventura.timesheet.util.SharedPrefManager
import android.content.Context


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest

import kotlinx.android.synthetic.main.activity_login.*
import www.sanju.motiontoast.MotionToast
import com.google.android.material.snackbar.Snackbar

import com.android.volley.VolleyError
import org.json.JSONObject
import java.lang.reflect.Method
import java.util.HashMap


class LoginActivity : AppCompatActivity() {
    // lateinit var bt_login: Button
    lateinit var etUser: EditText
    lateinit var etPassword: EditText
    lateinit var ctx: LoginActivity
    lateinit var manager: SharedPrefManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)
        ctx = this
        manager = SharedPrefManager(ctx)
        initView()
        initValue()

    }

    private fun initView() {
        // bt_login = findViewById(R.id.bt_login)

        etUser = findViewById(R.id.etUser)
        etPassword = findViewById(R.id.etPassword)
    }

    private fun initValue() {
        bt_login.setOnClickListener { view ->
            if (validateTools()) {
                getLoginUser()
            } else {
                MotionToast.createToast(
                    this,
                    null, "Please Fill All Details !",
                    MotionToast.TOAST_INFO,
                    MotionToast.GRAVITY_BOTTOM,
                    MotionToast.LONG_DURATION, null
                )
            }
        }

    }

    private fun getLoginUser() {
        Log.e("login_service", "BaseUrl>" + NEW_LOGIN + "MobileApi/login_api")
        val stringRequest: StringRequest = object : StringRequest(Request.Method.POST,
            NEW_LOGIN + "MobileApi/login_api", object : Response.Listener<String> {
                override fun onResponse(response: String?) {
                    Log.e("login_service", "params>$response")
                    try {
                        var jsonObject = JSONObject("" + response)
                        var status = jsonObject.getString("status");
                        if (jsonObject.getString("status").equals("success", ignoreCase = true)) {
                            manager.setEmploye_Role(jsonObject.getString("role").toString())


                            manager.setEmploye_Details(jsonObject.getString("role").toString(),)
                            Log.e("login_service", "role>${manager.getEmploye_Role()}")
                           startActivity(Intent(ctx, DashbordMainActivity::class.java))
                        } else {
                            MotionToast.createToast(
                                ctx,
                                null, status,
                                MotionToast.TOAST_ERROR,
                                MotionToast.GRAVITY_BOTTOM,
                                MotionToast.LONG_DURATION, null
                            )
                        }


                    } catch (ex: Exception) {

                    }
                }
            }, object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError?) {
                    Log.e("login_service", "params>${error?.message}")
                    MotionToast.createToast(
                        ctx,
                        null, error?.message.toString(),
                        MotionToast.TOAST_ERROR,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION, null
                    )
                }

            }) {
            override fun getParams(): Map<String, String>? {
                val params: MutableMap<String, String> = HashMap()
                params["user_id"] = etUser.text.toString()
                params["password"] = etPassword.text.toString()
                Log.e("login_service", "params>$params")
                return params
            }

        }

        VolleySingleton(applicationContext).addToRequestQueue(stringRequest);
    }

    private fun validateTools(): Boolean {
        var flag = false
        if (etUser.text.equals("") || etUser.text.length == 0) {
            flag = false
            MotionToast.createToast(
                this,
                null, "Invaldate User !",
                MotionToast.TOAST_ERROR,
                MotionToast.GRAVITY_BOTTOM,
                MotionToast.LONG_DURATION, null
            )
        } else if (etPassword.text.equals("") || etPassword.text.length == 0) {
            flag = false
            MotionToast.createToast(
                this,
                null, "Invaldate Password !",
                MotionToast.TOAST_ERROR,
                MotionToast.GRAVITY_BOTTOM,
                MotionToast.LONG_DURATION, null
            )
        } else {
            flag = true
        }

        return flag
    }


}