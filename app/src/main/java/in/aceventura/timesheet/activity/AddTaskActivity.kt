package `in`.aceventura.timesheet.activity

import `in`.aceventura.timesheet.R
import `in`.aceventura.timesheet.adapter.ProjectAdapter
import `in`.aceventura.timesheet.model.ProjectModel
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_add_task.*

class AddTaskActivity : AppCompatActivity() {

    var Type = "P";
    lateinit var rv_ProjectList: RecyclerView
    lateinit var rv_OverheadList: RecyclerView
    var mProjectList = ArrayList<ProjectModel>()
    var modelProject = ProjectModel();
    lateinit var mprojectAdapter: ProjectAdapter;
    lateinit var projectListLayoutManager: LinearLayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task)


        lv_Project.setOnClickListener { view ->
            Type = "P"
            rv_ProjectList = findViewById(R.id.rv_ProjectList)
            projectListLayoutManager = LinearLayoutManager(this)
            rv_ProjectList.layoutManager = projectListLayoutManager
            modelProject.projectname = "aaa"
            modelProject.projectask = ""
            modelProject.projecdescription = ""
            modelProject.projectime = ""
            mProjectList.add(modelProject)
            mprojectAdapter = ProjectAdapter(mProjectList, this)

            rv_ProjectList.adapter = mprojectAdapter

            lv_Overhead.setBackgroundResource(R.drawable.custom_card_round_baground_grey)
            lv_Project.setBackgroundResource(R.drawable.custom_card_round_baground_black)
            lv_ProjectView.visibility = View.VISIBLE

        }
        lv_Overhead.setOnClickListener { view ->
            Type = "O"
            rv_OverheadList = findViewById(R.id.rv_OverheadList)
            rv_ProjectList = findViewById(R.id.rv_ProjectList)

            lv_Project.setBackgroundResource(R.drawable.custom_card_round_baground_grey)
            lv_Overhead.setBackgroundResource(R.drawable.custom_card_round_baground_black)
            lv_ProjectView.visibility = View.GONE
            rv_ProjectList.visibility = View.GONE
        }
        tv_addtask.setOnClickListener {

            startActivity(Intent(this, DashbordMainActivity::class.java))
            finish()

        }
    }
}