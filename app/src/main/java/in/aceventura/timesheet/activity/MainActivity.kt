@file:Suppress("UNREACHABLE_CODE")

package `in`.aceventura.timesheet.activity

import `in`.aceventura.timesheet.R
import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception


class MainActivity : AppCompatActivity() {
    // inside a basic activity
    private var locationManager: LocationManager? = null
    var lat = ""
    var long = ""
    private val PERMISSION_REQUEST_CODE = 200
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Create persistent LocationManager reference
        initView()
        initValue()


    }

    //add Listener here
    private fun initValue() {

        bt_getLocation.setOnClickListener { view ->
            try {
                // Request location updates

                Log.e("location", "values{latitude$lat,long$long}")
                tv_location.text = ("<<latitude=>>" + lat + "<<longitude=>>" + long)

            } catch (ex: SecurityException) {
                Log.e("location", "Security Exception, no location available")
            }
        }
    }

    //define init value
    private fun initView() {
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        permission()

        try {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                permission();
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            locationManager?.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                0L,
                0f,
                locationListener

            );
        } catch (exx: Exception) {
            exx.printStackTrace()

        }
    }

    // dynamic permition
    private fun permission() {

        Log.e("location", "values{latitude$lat,long$long}")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.e("location", "inside M")
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                PERMISSION_REQUEST_CODE
            )

        } else {
            Log.e("location", "out M")

        }

    }


    //define the listener
    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            lat = "" + location.longitude
            long = "" + location.latitude

        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }
}