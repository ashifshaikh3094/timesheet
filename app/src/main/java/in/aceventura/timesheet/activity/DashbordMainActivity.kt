package `in`.aceventura.timesheet.activity

import `in`.aceventura.timesheet.R
import `in`.aceventura.timesheet.adapter.DailyAttendanceAdapter
import `in`.aceventura.timesheet.adapter.DailyAttendanceAproveRejectAdapter
import `in`.aceventura.timesheet.model.DailyModelList
import `in`.aceventura.timesheet.util.SharedPrefManager
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.topbar.*

class DashbordMainActivity : AppCompatActivity() {
    lateinit var rv_taskList: RecyclerView

    lateinit var manager: SharedPrefManager

    lateinit var ctx: Activity
    lateinit var mprojectAdapter: DailyAttendanceAdapter;
    var modelListArray = ArrayList<DailyModelList>()
    lateinit var projectListLayoutManager: LinearLayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashbord_main)
        drawer.setOnClickListener { view ->
            startActivity(Intent(this, NavigationDashbordActivity::class.java))

        }
        ctx = this
        manager = SharedPrefManager(ctx)
        initView()

    }

    private fun initView() {



        Log.e("login_service", "Dashboardrole>${manager.getEmploye_Role()}")
        rv_taskList = findViewById(R.id.rv_taskList)
        projectListLayoutManager = LinearLayoutManager(this)
        rv_taskList.layoutManager = projectListLayoutManager
         /*var modelList :DailyModelList()
        modelList.employeeCode = "12";
        modelList.employeeName = "abc abcdef"
        modelList.employeeTaskName = "Logic appling(Overhead)"
        modelList.date = "13/09/2021"
        modelList.hours = "01.02"*/
        modelListArray.add(DailyModelList("12","abc abcdef","Logic appling(Overhead)","13/09/2021","01.02"))
        modelListArray.add(DailyModelList("13","Roshan Unawane","Logic appling","13/09/2021","01.02"))
        modelListArray.add(DailyModelList("14","Yogesh khankar","Design Maintanin","13/09/2021","01.02"))
        modelListArray.add(DailyModelList("15","Lalit Pathade","Coding Web Services","13/09/2021","01.02"))
        modelListArray.add(DailyModelList("16","Ashif","Time Sheet Design ","13/09/2021","01.02"))
        modelListArray.add(DailyModelList("17","Sanket Shinde","Offline Leave ","13/09/2021",""))
        mprojectAdapter = DailyAttendanceAdapter(modelListArray, this)

        rv_taskList.adapter = mprojectAdapter
    }
}