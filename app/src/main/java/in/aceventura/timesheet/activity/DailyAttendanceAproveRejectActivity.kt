package `in`.aceventura.timesheet.activity

import `in`.aceventura.timesheet.R
import `in`.aceventura.timesheet.adapter.DailyAttendanceAdapter
import `in`.aceventura.timesheet.adapter.DailyAttendanceAproveRejectAdapter
import `in`.aceventura.timesheet.model.DailyModelList
import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class DailyAttendanceAproveRejectActivity : AppCompatActivity() {
   lateinit var cntx: Activity
   lateinit var rv_approveReject:RecyclerView
    lateinit var sp_ModeSelection:Spinner

    lateinit var mprojectAdapter: DailyAttendanceAproveRejectAdapter;
    var modelListArray = ArrayList<DailyModelList>()
    lateinit var projectListLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daily_attendance_aprove_reject)
        cntx=this;
        initView()
    }

    private fun initView() {
        rv_approveReject=findViewById(R.id.rv_approveReject)
        sp_ModeSelection=findViewById(R.id.sp_ModeSelection)
        val values : Array<String> = arrayOf("Pending", "Approved")

        sp_ModeSelection.adapter = ArrayAdapter<String>(cntx, android.R.layout.simple_list_item_1, values)

        projectListLayoutManager = LinearLayoutManager(this)
        rv_approveReject.layoutManager = projectListLayoutManager
        /*var modelList :DailyModelList()
       modelList.employeeCode = "12";
       modelList.employeeName = "abc abcdef"
       modelList.employeeTaskName = "Logic appling(Overhead)"
       modelList.date = "13/09/2021"
       modelList.hours = "01.02"*/
        modelListArray.add(DailyModelList("12","abc abcdef","Logic appling(Overhead)","13/09/2021","01.02"))
        modelListArray.add(DailyModelList("13","Roshan Unawane","Logic appling","13/09/2021","01.02"))
        modelListArray.add(DailyModelList("14","Yogesh khankar","Design Maintanin","13/09/2021","01.02"))
        modelListArray.add(DailyModelList("15","Lalit Pathade","Coding Web Services","13/09/2021","01.02"))
        modelListArray.add(DailyModelList("16","Ashif","Time Sheet Design ","13/09/2021","01.02"))
        modelListArray.add(DailyModelList("17","Sanket Shinde","Offline Leave ","13/09/2021",""))
        mprojectAdapter = DailyAttendanceAproveRejectAdapter(modelListArray, this)

        rv_approveReject.adapter = mprojectAdapter

    }
}