package `in`.aceventura.timesheet.activity

import `in`.aceventura.timesheet.R
import `in`.aceventura.timesheet.adapter.DailyTaskAddAdapter
import `in`.aceventura.timesheet.adapter.ViewDetailsAdaper
import `in`.aceventura.timesheet.model.DailyModelList
import `in`.aceventura.timesheet.model.DailyTaskModel
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ViewDetailsActivity : AppCompatActivity() {

  lateinit var  rv_DViewDetails:RecyclerView

    lateinit var mprojectAdapter: ViewDetailsAdaper;
    var listDailyTaskModel = ArrayList<DailyModelList>()
    var hoursdummy = ArrayList<String>()
    lateinit var projectListLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_details)
        initView();
    }

    private fun initView() {
        rv_DViewDetails=findViewById(R.id.rv_DViewDetails)
        projectListLayoutManager = LinearLayoutManager(this)
        rv_DViewDetails.layoutManager = projectListLayoutManager

        var mdailyTaskModel = DailyModelList();
        mdailyTaskModel.hours = ""

        listDailyTaskModel.add(mdailyTaskModel)

        mprojectAdapter = ViewDetailsAdaper(listDailyTaskModel, this)

        rv_DViewDetails.adapter = mprojectAdapter



    }
}