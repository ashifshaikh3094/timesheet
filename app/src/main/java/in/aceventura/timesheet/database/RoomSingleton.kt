package `in`.aceventura.timesheet.database

import `in`.aceventura.timesheet.model.DailyTaskModel
import `in`.aceventura.timesheet.model.dailyTaskDao
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
@Database(entities = arrayOf(DailyTaskModel::class), version = 1)
abstract class RoomSingleton: RoomDatabase() {

    abstract fun dailyTaskDao(): dailyTaskDao

    companion object {
        private var INSTANCE: RoomSingleton? = null
        fun getInstance(context: Context): RoomSingleton {
            if (INSTANCE == null) {
                 INSTANCE = Room.databaseBuilder(
                    context,
                    RoomSingleton::class.java,
                    "roomdb").allowMainThreadQueries().build()

            }
            return INSTANCE as RoomSingleton
        }
    }
}