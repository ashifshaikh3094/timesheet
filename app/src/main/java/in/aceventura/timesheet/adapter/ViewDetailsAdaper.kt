package `in`.aceventura.timesheet.adapter

import `in`.aceventura.timesheet.R
import `in`.aceventura.timesheet.activity.DailyAttendanceAproveRejectActivity
import `in`.aceventura.timesheet.model.DailyModelList
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_list_item.view.*
import java.util.ArrayList

class ViewDetailsAdaper (var dailyModelList: ArrayList<DailyModelList>, val cntx: Context) :
    RecyclerView.Adapter<ViewDetailsAdaper.MyHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        return MyHolder(
            LayoutInflater.from(cntx).inflate(R.layout.view_list_item, parent, false)
        )
    }

    override fun getItemCount() = dailyModelList.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyHolder, position: Int) {


        holder.itemView.setOnClickListener { view->


        }



    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {



    }

}