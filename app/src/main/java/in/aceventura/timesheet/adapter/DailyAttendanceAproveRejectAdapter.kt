package `in`.aceventura.timesheet.adapter

import `in`.aceventura.timesheet.R
import `in`.aceventura.timesheet.activity.DailyAttendanceAproveRejectActivity
import `in`.aceventura.timesheet.activity.ViewDetailsActivity
import `in`.aceventura.timesheet.model.DailyModelList
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.aprove_reject_list_item.view.*
import kotlinx.android.synthetic.main.daily_list_item.view.*

import java.util.ArrayList



class DailyAttendanceAproveRejectAdapter(
    var dailyModelList: ArrayList<DailyModelList>,
    val cntx: Context
) :
    RecyclerView.Adapter<DailyAttendanceAproveRejectAdapter.MyHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        return MyHolder(
            LayoutInflater.from(cntx).inflate(R.layout.aprove_reject_list_item, parent, false)
        )
    }

    override fun getItemCount() = dailyModelList.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyHolder, position: Int) {


        /*  holder.tv_employeeTaskName.text="Task Name :"+dailyModelList[position].employeeTaskName
          holder.tvDate.text="Date :"+dailyModelList[position].date
          holder.tvHours.text="Hours :"+dailyModelList[position].hours*/

        holder.tv_ViewDetails.setOnClickListener { view ->

            cntx.startActivity(Intent(cntx, ViewDetailsActivity::class.java))

        }


    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_ViewDetails = itemView.tv_ViewDetails

    }

}