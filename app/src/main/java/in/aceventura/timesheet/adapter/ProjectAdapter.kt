package `in`.aceventura.timesheet.adapter

import `in`.aceventura.timesheet.R
import `in`.aceventura.timesheet.model.ProjectModel
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.project_list_item.view.*

import java.util.ArrayList;

public class ProjectAdapter(var achivement: ArrayList<ProjectModel>, val cntx: Context) :
    RecyclerView.Adapter<ProjectAdapter.MyHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        return ProjectAdapter.MyHolder(
            LayoutInflater.from(cntx).inflate(R.layout.project_list_item, parent, false)
        )
    }

    override fun getItemCount() = achivement.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyHolder, position: Int) {

        val values : Array<String> = arrayOf("USD", "UAH", "GBD", "EUR", "BIT", "RUB")
      /*  var data = ArrayList<String>()
        data.add("USD")
        data.add("RUB")*/

        holder.sp_projectName.adapter = ArrayAdapter<String>(cntx, android.R.layout.simple_list_item_1, values)
        holder.sp_projectTask.adapter = ArrayAdapter<String>(cntx, android.R.layout.simple_list_item_1, values)


    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var sp_projectName = itemView.sp_projectName
        var sp_projectTask = itemView.sp_projectTask
        var tv_time = itemView.tv_time
    }

}