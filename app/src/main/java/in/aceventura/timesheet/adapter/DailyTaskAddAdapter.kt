package `in`.aceventura.timesheet.adapter

import `in`.aceventura.timesheet.R
import `in`.aceventura.timesheet.database.RoomSingleton
import `in`.aceventura.timesheet.model.DailyTaskModel
import `in`.aceventura.timesheet.util.AllValues
import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.taskadd_list_item.view.*
import kotlin.collections.ArrayList

class DailyTaskAddAdapter(
    var dailyModelList: ArrayList<DailyTaskModel>,
    var daily: ArrayList<String>,
    var cntx: Context,
    java: AllValues,


    ) :
    RecyclerView.Adapter<DailyTaskAddAdapter.MyHolder>() {
    var hour = ArrayList<String>();
    var selectedText = ""
    private lateinit var mDb: RoomSingleton
    var dd: AllValues = java

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {

        return MyHolder(
            LayoutInflater.from(cntx).inflate(R.layout.taskadd_list_item, parent, false)
        )
    }

    override fun getItemCount() = dailyModelList.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        mDb = RoomSingleton.getInstance(cntx)
        var mdailyTaskModel = dailyModelList[position];

        holder.et_taskDes.setText(mdailyTaskModel.dailyEditableTask)
        // holder.tv_taskDes.text=dailyModelList[position].dailyEditableTask
        val job: Array<String> = arrayOf("Select Job")
        val task: Array<String> = arrayOf("Select Task")
        val hours: Array<String> =
            arrayOf("Select Hours", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10")
        holder.sp_addJob.adapter =
            ArrayAdapter<String>(cntx, android.R.layout.simple_list_item_1, job)
        holder.sp_addtask.adapter =
            ArrayAdapter<String>(cntx, android.R.layout.simple_list_item_1, task)
        holder.sp_addhours.adapter =
            ArrayAdapter<String>(cntx, android.R.layout.simple_list_item_1, hours)


        holder.sp_addhours.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                selectedText = parent.getItemAtPosition(position).toString()
                ;

                Toast.makeText(
                    cntx,
                    "Values" + parent.getItemAtPosition(position).toString(),
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }




       /* //  aa.addHour(selectedText)
        Log.e("Varify", "datasaving?=" + selectedText)

        mdailyTaskModel.dailyhours = selectedText
        Log.e("dberror", "datasavingmodel055?=" + selectedText)
        mdailyTaskModel.dailyid=""+(0..100).random()
        Log.e("dberror", "datasavingmodel055?=" +(0..100).random())
        if(selectedText.length>0){
            Log.e("dberror", "datasavingmodel055?=" +(0..100).random())

            mDb.dailyTaskDao().insert(mdailyTaskModel)
            Log.e("dberror", "datasavingmodel0555?=" +(0..100).random())

        }*/
        daily.addAll(hour)
        dd.addHour(mdailyTaskModel.dailyhours)
        collectionAllObject(daily)

        /*  try {
              Log.e("dberror", "datasaving?=" + selectedText)

                           Log.e("dberror", "datasavingmodel?=" + selectedText)

  */                  Log.e("dberror", "datasavingmodel000?=" + selectedText)



        Log.e("dberror", "datasavingmodel001?=" + selectedText)


    }

    public fun collectionAllObject(daily: ArrayList<String>): ArrayList<String> {
        return daily;
    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var et_taskDes = itemView.et_taskDes
        var sp_addJob = itemView.sp_addJob
        var sp_addtask = itemView.sp_addtask
        var sp_addhours = itemView.sp_addhours


    }

}