package`in`.aceventura.timesheet.adapter

import `in`.aceventura.timesheet.R
import `in`.aceventura.timesheet.activity.DailyAttendanceAproveRejectActivity
import `in`.aceventura.timesheet.activity.DashbordMainActivity
import `in`.aceventura.timesheet.activity.MainActivity
import `in`.aceventura.timesheet.model.DailyModelList
import `in`.aceventura.timesheet.model.ProjectModel
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.daily_list_item.view.*
import kotlinx.android.synthetic.main.project_list_item.view.*
import java.util.ArrayList

public class DailyAttendanceAdapter(var dailyModelList: ArrayList<DailyModelList>, val cntx: Context) :
    RecyclerView.Adapter<DailyAttendanceAdapter.MyHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        return MyHolder(
            LayoutInflater.from(cntx).inflate(R.layout.daily_list_item, parent, false)
        )
    }

    override fun getItemCount() = dailyModelList.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyHolder, position: Int) {

        holder.tv_employeeid.text="Employee Code:"+dailyModelList[position].employeeCode
        holder.tv_employeeName.text="Employee Name: "+dailyModelList[position].employeeName
        holder.tv_employeeTaskName.visibility=View.GONE
        holder.tvDate.visibility=View.GONE
        holder.tvHours.visibility=View.GONE
      /*  holder.tv_employeeTaskName.text="Task Name :"+dailyModelList[position].employeeTaskName
        holder.tvDate.text="Date :"+dailyModelList[position].date
        holder.tvHours.text="Hours :"+dailyModelList[position].hours*/

        holder.itemView.setOnClickListener { view->

            cntx.startActivity(Intent(cntx, DailyAttendanceAproveRejectActivity::class.java))

        }



    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tv_employeeid = itemView.tv_employeeid
        var tv_employeeName = itemView.tv_employeeName
        var tv_employeeTaskName = itemView.tv_employeeTaskName
        var tvDate = itemView.tvDate
        var tvHours = itemView.tvHours

    }

}