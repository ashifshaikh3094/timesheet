package `in`.aceventura.timesheet.util

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.util.Log

class SharedPrefManager {
    private var mCtx: Context? = null
    val KEY_Employe_Role = "role"
    val KEY_Employe_code = "emp_code"
    val KEY_Employe_name = "emp_name"
    val KEY_Employe_department = "department"
    val KEY_Employe_reportingTo = "reportingTo"
    val KEY_User_name = "user_name"
    val KEY_User_Password = "user_password"
    val TAG = "timesheetsf"

    lateinit var sf: SharedPrefManager
    private lateinit var _prefs: SharedPreferences
    lateinit var editor: Editor

    constructor(ctx: Context) {
        this.mCtx = mCtx;
        mInstance = this
        _prefs = ctx.getSharedPreferences("timesheet", Context.MODE_PRIVATE)
        val sharedPreferences = _prefs
        editor = sharedPreferences.edit()

    }

    companion object {
        @JvmStatic
        lateinit var mInstance: SharedPrefManager

        @JvmStatic
        var SHARED_PREF_NAME = "timesheet"
    }

    init {
        mInstance = this
    }

    @Synchronized
    fun getInstance(context: Context?): SharedPrefManager? {
        if (mInstance == null) {
            mInstance = SharedPrefManager(context!!)
        }
        return mInstance
    }


    fun setEmploye_Role(role: String?,) {
        Log.e(TAG, "Addrole==>" + role)
        editor.putString(mInstance.KEY_Employe_Role, role)
        editor.apply()
        Log.e(TAG, "Finishrole==>" + role)
    }

    fun getEmploye_Role(): String {
        return _prefs.getString(mInstance.KEY_Employe_Role, "").toString();
    }

    fun setEmploye_Details(toString: String) {

    }


}