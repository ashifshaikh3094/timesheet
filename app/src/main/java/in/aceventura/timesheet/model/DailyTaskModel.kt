package `in`.aceventura.timesheet.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "dailyTaskModeltbl")
class DailyTaskModel {
    @PrimaryKey
    var dailyid: String = ""

    @ColumnInfo(name = "dailyJobSelector")
    var dailyJobSelector: String = ""

    @ColumnInfo(name = "dailyJobTask")
    var dailyJobTask: String = ""

    @ColumnInfo(name = "dailyhours")
    var dailyhours: String = ""

    @ColumnInfo(name = "dailyEditableTask")
    var dailyEditableTask: String = ""

    constructor() {

    }

    override fun toString(): String {
        return "DailyTaskModel(dailyid='$dailyid', dailyJobSelector='$dailyJobSelector', dailyJobTask='$dailyJobTask', dailyhours='$dailyhours', dailyEditableTask='$dailyEditableTask')"
    }


}