package `in`.aceventura.timesheet.model

import androidx.room.PrimaryKey

public  class DailyModelList {


    var employeeCode: String = ""
    var employeeName: String = ""
    var employeeTaskName: String = ""
    var date: String = ""
    var hours: String = ""

    constructor(
        employeeCode: String,
        employeeName: String,
        employeeTaskName: String,
        date: String,
        hours: String
    ) {
        this.employeeCode = employeeCode
        this.employeeName = employeeName
        this.employeeTaskName = employeeTaskName
        this.date = date
        this.hours = hours
    }

    constructor(){

    }

}