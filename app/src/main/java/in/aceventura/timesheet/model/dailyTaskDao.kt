package `in`.aceventura.timesheet.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface dailyTaskDao {
    @Query("SELECT * FROM dailyTaskModeltbl")
    fun allStudents():List<DailyTaskModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(student:DailyTaskModel)

}