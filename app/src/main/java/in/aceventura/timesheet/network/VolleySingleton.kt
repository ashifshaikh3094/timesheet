package `in`.aceventura.timesheet.network

import android.content.Context
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley


class VolleySingleton {

    private var mInstance: VolleySingleton? = null
    private var mRequestQueue: RequestQueue? = null
    private var mContext: Context? = null


    constructor(context: Context) {
        // Specify the application context
        mContext = context
        // Get the request queue
        mRequestQueue = getRequestQueue()
    }

    @Synchronized
    fun  getInstance(context: Context?): VolleySingleton? {
        // If Instance is null then initialize new Instance
        if (mInstance == null) {
            mInstance = context?.let { VolleySingleton(it) }
        }
        // Return MySingleton new Instance
        return mInstance
    }

    private fun getRequestQueue(): RequestQueue? {
        // If RequestQueue is null the initialize new RequestQueue
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext?.applicationContext);
        }

        // Return RequestQueue
        return mRequestQueue;
    }

    fun <T> addToRequestQueue(request: Request<T>?) {
        // Add the specified request to the request queue
        request?.setRetryPolicy(
            DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ))
        getRequestQueue()!!.add(request)
    }

}